/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tic.tac.toe;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class OX_Programm {
    static int count = 0;
    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void showinput() {
        while (true) {
            System.out.println("Please input Row Col :");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table[row][col] == '-') {
                table[row][col] = player;
                count++;
                break;
            }
            System.out.println("Error");

        }
        showTable();

    }

    static void showSwitchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void diagonal() {

        if (table[0][0] == 'X') {
            if (table[1][1] == 'X') {
                if (table[2][2] == 'X') {
                    isFinish = true;
                    winner = player;
                }
            }

        }if (table[0][2] == 'X') {
            if (table[1][1] == 'X') {
                if (table[2][0] == 'X') {
                    isFinish = true;
                    winner = player;
                }
            }
        }if (table[0][0] == 'O') {
            if (table[1][1] == 'O') {
                if (table[2][2] == 'O') {
                    isFinish = true;
                    winner = player;
                }
            }
        }
        if (table[0][2] == 'O') {
            if (table[1][1] == 'O') {
                if (table[2][0] == 'O') {
                    isFinish = true;
                    winner = player;
                }
            }
        }
    } static void checkDraw(){
        if(winner == '-' && count == 9){
            isFinish = true;
        }
    }

    static void checkWin(){
        checkRow();
        checkCol();
        diagonal();
        checkDraw();
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println("Player" + " " + winner + " Win ...");
        }

    }

    public static void main(String[] args) {
        System.out.println("Welcome to OX Game");
        do {
            showTable();
            showTurn();
            showinput();
            checkWin();
            showSwitchPlayer();
        } while (!isFinish);
        showResult();
        System.out.println("Bye bye . . . .");
    }

}
